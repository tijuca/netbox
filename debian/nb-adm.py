#!/usr/bin/python3

# SPDX-License-Identifier: Apache-2.0
# SPDXVersion: SPDX-2.2
# SPDX-FileCopyrightText: Copyright 2021 Contributors of the NetBox Debian package

"""
This script is indented to get used as an administration interface for
managing an NetBox installation.
But it's also used while the apt/dpkg package setup process and wraps the
necessary steps during the postinstall of the netbox package.

It checks whether some preconditions are met, then executes several actions.
These actions are currently:
  SECRETKEY: generate a new and secure SECRET_KEY value and replace the
             existing one in the configuration
  MIGRATE:   execute migrate command in order to update the database schema
  COLLECTSTATIC: execute a collectstatic command against the installation

All configuration is read from within this file, change the values here if
you need to change configuration.
"""

# TODO: make the actions separately executable
# TODO: create helpful output and sub-command documentation

import re
import os
import subprocess
import sys
from traceback import print_exc

# these variables determine which user and group must exist, and to whom all
# files we generate will belong
import secrets

USER = "netbox"
GROUP = "netbox"

# for action SECRET KEY
# the file where a secret key should be created
CONFIG_FOLDER = "/etc/netbox"
CONFIG_FILE = "netbox-config.py"

# for action MIGRATE
# this is the call for executing the migrate action
MIGRATE_CALL = (
    'cd /usr/share/netbox/netbox && su -s /bin/bash -c "python3 ./manage.py migrate" -g netbox netbox'
)

# for action COLLECTSTATIC
# this is the call for executing the collectstatic action
COLLECTSTATIC_CALL = (
    'cd /usr/share/netbox/netbox && su -s /bin/bash -c "python3 ./manage.py collectstatic --noinput" -g netbox netbox'
)


def check_preconditions():
    """
    Make sure all general preconditions are met.

    Specifically: the specified user/group combination exists
    """

    # check for write permissions in /etc/netbox
    if not os.access('/etc/netbox', os.W_OK):
        print(f"ERROR: Can't access the configuration folder '{CONFIG_FOLDER}' with write permissions!")
        sys.exit(1)

    # check if the config file exists
    if not os.path.isfile(f"{CONFIG_FOLDER}/{CONFIG_FILE}"):
        print(f"ERROR: Required configuration file '{CONFIG_FOLDER}/{CONFIG_FILE}' not found!")
        sys.exit(1)

    # check for existing user
    check_user = check_user_exists(USER)
    if not check_user:
        print(f"The user '{USER}' does not exist!")
        return False
    username, uid, gid = check_user

    check_group = check_group_exists(GROUP)
    if not check_group:
        print(f"The group '{GROUP}' does not exist!")
        return False
    groupname, group_id = check_group

    if gid != group_id:
        print(f"WARNING: The user '{username}' does not have '{groupname}' as primary group!")

    return True


def check_user_exists(username):
    """
    Check that a user with the given username exists,
    return (username, uid, gid) if so, None otherwise.
    """

    users_output = subprocess.run(["/usr/bin/getent", "passwd"], capture_output=True).stdout.decode("utf-8").strip()
    for user_info in users_output.split("\n"):
        username_ent, _, uid, gid, _, _, _ = user_info.split(":")
        if username == username_ent:
            return username, uid, gid

    return None


def check_group_exists(groupname):
    """
    Check that a group with the given groupname exists,
    return (groupname, gid) if so, None otherwise.
    """
    group_output = subprocess.run(["/usr/bin/getent", "group"], capture_output=True).stdout.decode("utf-8").strip()
    for group_info in group_output.split("\n"):
        groupname_ent, _, gid, _ = group_info.split(":")
        if groupname == groupname_ent:
            return groupname, gid

    return None


def do_create_secret_key():
    """
    Execute the SECRETKEY action:
     create a new secure secret key and place it in the correct
     configuration file.
    """
    print("  executing SECRETKEY...")
    success = True

    with open(f"{CONFIG_FOLDER}/{CONFIG_FILE}", "r") as f:
        lines = f.readlines()

    found_lines = 0

    # create new secure secret key
    secret_key = secrets.token_urlsafe(64)

    # find the line with the secret key
    rex = re.compile(r"^SECRET_KEY\s*=\s*['\"]")
    for i in range(len(lines)):
        line = lines[i]
        if rex.match(line):
            # replace the secret key line
            lines[i] = "SECRET_KEY = '{}'\n".format(secret_key)
            found_lines += 1

    with open(f"{CONFIG_FOLDER}/{CONFIG_FILE}", "w") as f:
        f.writelines(lines)

    if found_lines != 1:
        print(f"WARNING: Found {found_lines} lines with secret keys!")

    print("  finished SECRETKEY")
    return success

def do_makemigration():
    """
    Execute the MIGRATE action:
     call a script that does a db migrate and check its output.
    """

    print("  executing MIGRATE...")
    process = subprocess.run(MIGRATE_CALL, shell=True, capture_output=True)
    if process.returncode != 0:
        print("WARNING: MIGRATE call did not return successfully")
        print(f"The following command call went wrong:\n'{MIGRATE_CALL}'\n")
        print(process.stderr.decode('utf-8'))
        return False

    print("  finished MIGRATE")
    print(process.stdout.decode('utf8'))
    return True


def do_collectstatic():
    """
    Execute the COLLECTSTATIC action:
     call a script that does a collectstatic and check its output.
    """

    print("  executing COLLECTSTATIC...")
    process = subprocess.run(COLLECTSTATIC_CALL, shell=True, capture_output=True)
    if process.returncode != 0:
        print("WARNING: collectstatic call did not return successfully")
        print(process.stderr.decode('utf-8'))
        return False

    print("  finished COLLECTSTATIC")
    print(process.stdout.decode('utf8'))
    return True


def main():
    # TODO: parameter parsing for sub-commands and help output
    do_create_secret_key()
    if do_makemigration() == False:
        sys.exit(1)
    if do_collectstatic() == False:
        sys.exit(1)


if __name__ == "__main__":
    try:
        preconditions = check_preconditions()
    except Exception as e:
        print_exc(e)
        preconditions = False
    if not preconditions:
        print("ERROR: Not all preconditions for execution were present!")
        sys.exit(1)
    else:
        main()
