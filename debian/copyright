Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: NetBox
Upstream-Contact: Jeremy Stretch <jstretch@ns1.com>
Source: https://github.com/netbox-community/netbox
Comment: Upstream ships (as usual in such projects) some precompiled and
 minimized CSS and JS code which we are currently unable to rebuild
 completely from the available tools and packages within the Debian main
 archive. This makes the resulting binary package unfortunately non-free.
 .
 Upstream is providing a script in scripts/verify-bundles.sh that is doing
 two things, step one is to catch up all required libraries by using the
 tool 'yarn' and build up all the things, step two is verifying the build
 result.
 .
 If someone can help to analyze step one of the script and create a proper
 workflow for the packaging the netbox package please get in contact with the
 package maintainers!

Files: *
Copyright: NetBox Community
 DigitalOcean, LLC
License: Apache-2.0

Files: debian/*
Copyright: 2021-2024, Carsten Schoenert <c.schoenert@t-online.de>
License: Apache-2.0

Files: debian/missing-sources/clipboard.js
Copyright: Zeno Rocha
License: MIT
Comment: The upstream project is https://clipboardjs.com/. The source can be
 be found on https://github.com/zenorocha/clipboard.js/.

Files: debian/missing-sources/flatpickr.js
Copyright: 2017-* Gregory Petrosyan
 Microsoft Corporation
License: Apache-2.0 and MIT
Comment: The original base of that file is version 4.6.3 based on
 https://github.com/flatpickr/flatpickr/, the upstream project is located at
 https://flatpickr.js.org/. The used modified non minimized source file has
 some additions from Microsoft Corporation.

Files: debian/missing-sources/select2-bootstrap.css
Copyright: 2015-2017 Florian Kissling and contributors
License: MIT
Comment: The upstream source can be found on
 https://github.com/select2/select2-bootstrap-theme/

Files: netbox/project-static/dist/graphiql.js
Copyright: Sindre Sorhus
 Facebook, Inc. and its affiliates. (React v0.20.2 scheduler.production.min.js)
 Facebook, Inc. and its affiliates. (React v17.0.2 react-dom.production.min.js)
 Facebook, Inc. and its affiliates. (React v17.0.2 react.production.min.js)
License: MIT
Comment: This file is a minimized file created from various source files.
 https://github.com/facebook/react/tree/main/packages/scheduler
 https://github.com/facebook/react/tree/17.0.2/packages/react-dom
 https://github.com/facebook/react/tree/17.0.2/packages/react

Files: netbox/project-static/dist/netbox.js
Copyright: Zeno Rocha
 Microsoft Corporation
 2011-2021 The Bootstrap Authors (Bootstrap v5.0.2)
 David DeSandro - Masonry (Cascading grid layout library)
 David DeSandro - getsize (measure size of elements)
 Outlayer (the brains and guts of a layout library)
 2012-2014 Roman Shtylman (cookie)
 2015 Douglas Christopher Wilson (cookie)
License: Apache-2.0 and MIT
Comment: This file is a minimized file created from various source files.
 https://getbootstrap.com/
 https://masonry.desandro.com
 https://github.com/metafizzy/outlayer
 https://github.com/zenorocha/clipboard.js/
 https://github.com/jshttp/cookie
 https://github.com/desandro/get-size

Files: netbox/project-static/dist/materialdesignicons-webfont-*.woff
 netbox/project-static/dist/materialdesignicons-webfont-*.ttf
 netbox/project-static/dist/materialdesignicons-webfont-*.woff2
 netbox/project-static/dist/materialdesignicons-webfont-*.eot
Copyright: Google
License: Apache-2.0
Comment: Source available under https://github.com/marella/material-icons
 https://github.com/Templarian/MaterialDesign-Webfont

License: Apache-2.0
 On Debian systems, the complete text of Apache License, Version 2.0 can be
 found in '/usr/share/common-licenses/Apache-2.0'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
