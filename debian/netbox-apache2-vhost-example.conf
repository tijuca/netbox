# Apache2 vhost specific configuration for netbox
#vim: set ft=apache ts=4 sw=4 expandtab :#
#
# This vhost configuration requires the additional Apache2 modules:
#
#    mod_headers
#    mod_proxy
#    mod_proxy_http
#    mod_rewrite
#    mod_ssl
#    mod_http2
#    mod_proxy_http2
#
# These can be enabled by using a2enmod.
#
#    $ sudo a2enmod headers proxy proxy_http rewrite ssl
#    $ sudo a2enmod http2        # Strongly suggested, but not hardly necessary.
#    $ sudo a2enmod proxy_http2  # Required if http2 is enabled.

# Some definitions as we need to use the data multiple times.

Define RESOURCE_URI netbox.mydomain.org
Define SERVER_ADMIN admin@mydomain.org
# The port the gunicorn is awaiting connections, must be the same as in
# /etc/netbox/gunicorn.py specified!
Define PROXY_PORT 8001

Define SSL_FILE        /etc/ssl/certs/ssl-cert-snakeoil.pem
Define SSL_CERTIFICATE /etc/ssl/private/ssl-cert-snakeoil.key

<VirtualHost *:80>

    ServerName ${RESOURCE_URI}
    ServerAlias www.${RESOURCE_URI}
    ServerAdmin ${SERVER_ADMIN}

    # Rewrite the requests on port 80 and redirect the requests to port 443.
    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI}

</VirtualHost>

<VirtualHost *:443 >

    ServerName ${RESOURCE_URI}
    ServerAlias www.${RESOURCE_URI}
    ServerAdmin ${SERVER_ADMIN}

    RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}

    ProxyPreserveHost On
    ProxyRequests Off
    ProxyVia Off
    ProxyPass /static/ !
    ProxyPass / http://127.0.0.1:${PROXY_PORT}/
    ProxyPassReverse / http://127.0.0.1:${PROXY_PORT}/

    AliasMatch "^/static/?(.*)" "/var/lib/netbox/static/$1"
    <Directory /var/lib/netbox/static>
        AllowOverride None
        Options -Indexes +FollowSymLinks +MultiViews
        Require all granted
    </Directory>

    UseCanonicalName Off
    HostnameLookups On

    SSLEngine on
    SSLCipherSuite        ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNUL
    SSLCertificateFile    ${SSL_FILE}
    SSLCertificateKeyFile ${SSL_CERTIFICATE}

    <IfModule mod_headers.c>
        # HTTP Strict Transport Security (mod_headers is required) (63072000 seconds)
        Header always set Strict-Transport-Security "max-age=63072000"
    </IfModule>

    <IfModule mod_http2.c>
        # Enable HTTP/2, if available
        Protocols h2 http/1.1
    </IfModule>

    ErrorLog ${APACHE_LOG_DIR}/error.${RESOURCE_URI}.log
    CustomLog ${APACHE_LOG_DIR}/access.${RESOURCE_URI}.log combined

</VirtualHost>

